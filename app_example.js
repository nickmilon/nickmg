/**
 * example app
 */


const express = require('express');
const config = require('./config');

const { logger } = require('./modules/utils/logger');
const { MongoClientPlus, MongoEventsBasic } = require('./modules/mongo/client_plus');
const { EventLogger, sleep } = require('./modules/utils/delphi');
const { EndPointsMgAdmin } = require('./routes/mongo_admin');
const { EndPointsTests } = require('./routes/tests');
const { EndPointsMgUsage } = require('./routes/mongo_example');


const app = express();
app.set('json spaces', 40);
app.set('view engine', 'pug');

let server;
let clientMG;

const serverStart = async () => {
  logger.info({ Configuration: JSON.stringify(config, null, 4), NODE_ENV: process.env.NODE_ENV });
  try {
    clientMG = await new MongoClientPlus().connect(...config.dbWork);
    EventLogger(clientMG.client, MongoEventsBasic, 'mongoClient', logger, 0);
    server = app.listen(config.portAdmin, () => {
      EndPointsMgAdmin(app, clientMG);
      EndPointsTests(app, clientMG);
      EndPointsMgUsage(app, clientMG);
      logger.info(`server started Listening to http://localhost:${config.portAdmin}`);
    });
  } catch (err) {
    logger.error('app ERROR on starting up:', err);
    process.exit(1);
  }
};

const serverStop = async (reason = 'turned down') => {
  try {
    logger.info('server stopping ', reason);
    server.close();
    await clientMG.destroy(reason);
    await sleep(300);
    process.exit(0);
  } catch (err) {
    logger.error(err);
    process.exit(1);
  }
};

process.on('SIGINT', () => { serverStop('SIGINT'); });
serverStart();

