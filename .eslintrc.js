module.exports = {
    "extends": "airbnb",
    "rules": {
        "prefer-destructuring": ["error", {"object": false, "array": true}],
        "no-underscore-dangle": 0,
        "arrow-body-style": 0,
        // "quotes": ["error","double"],
        "max-len": [1, 120, 2, {ignoreComments: true}],
        "quote-props": [1, "consistent-as-needed"],
        "no-cond-assign": [2, "except-parens"],
        "radix": 0,
        "space-infix-ops": 0,
        "no-unused-vars": [1, {"vars": "local", "args": "none"}],
        "default-case": 0,
        "no-else-return": 0,
        "no-param-reassign": 0,
        "object-curly-newline": ["error", {"consistent": true}],
        "object-property-newline": ["error"],
        // "quotes": 0
      }
};