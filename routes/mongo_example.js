/**
 *
 * usage patterns examples
 * using Dependency Injection to acess db connections
 */


const expressExt = require('../modules/utils/express_ext');
const config = require('../config');
const { benchmarkWrites } = require('../modules/benchmarks/insUpd');

const EndPointsMgUsage = (app, dbc) => {
  const collTest = dbc.getCollection('test_nick', 'test');

  app.get('/usage/nop', (req, res) => {
    res.send('ok');
  });

  app.get('/usage/db/read/callback', (req, res) => {
    collTest.find({}).limit(100).toArray((err, results) => {
      if (err) {
        res.send(err);
      } else {
        res.send(results);
      }
    });
  });

  app.get('/usage/db/read/then', (req, res) => {
    collTest.find({}).limit(100).toArray()
      .then(result => res.send(result))
      .catch(err => res.send(err));
  });

  app.get('/usage/db/read/await', async (req, res) => {
    try { res.send(await collTest.find({}).limit(100).toArray()); } catch (err) { res.send(err); }
  });

  app.get('/usage/db/writeOne', async (req, res) => {
    try { res.send(await collTest.insertOne({ test: 'writeOne' }, { w: 1 })); } catch (err) { res.send(err); }
  });

  app.get('/usage/db/findOne', async (req, res) => {
    try {
      let result = await collTest.findOne({ test: 'writeOne' });
      if (result === null) { result = { colTest: 'empty' }; }
      res.send(result);
    } catch (err) { res.send(err); }
  });

  app.get('/usage/db/findMany/nextObj', async (req, res) => {
    const cursor = await collTest.find({});
    try {
      while (await cursor.hasNext()) { // eslint-disable-line 
        await cursor.next();           // eslint-disable-line
      }
    } catch (err) { res.send(err); }
    res.send({ ok: 1 });
  });

  app.get('/usage/db/findMany/nextObjThen', (req, res) => {
    // it only fetches 1st item needs a loop for many
    collTest.nextObject()
      .then(result => res.send(result))
      .catch(err => res.send(err));
  });

  app.get('/usage/db/findMany/UsingPipe', async (req, res) => {
    try {
      const cursor = await collTest.find({}, { limit: 4 });
      res.write('[');
      let prevChunk = null;
      cursor.on('data', async (data) => {
        if (prevChunk) { res.write(`${JSON.stringify(prevChunk)},`); }
        prevChunk = data;
      })
        .on('end', async () => {
          if (prevChunk) { res.write(JSON.stringify(prevChunk)); }
          res.end(']');
        })
        .on('error', async (err) => {
          res.end(err.message);
        });
    } catch (err) { res.send(err); }
  });

  app.get('/usage/db/findMany/UsingPipeSimple', async (req, res) => {
    try {
      const cursor = await collTest.find({}, { limit: 4 });
      cursor.on('data', async data => res.write(JSON.stringify(data)))
        .on('end', async () => res.end())
        .on('error', async (err) => {
          res.end(err.message);
        });
    } catch (err) { res.send(err); }
  });

  app.get('/usage/db/writeMany/:howMany/', async (req, res) => {
    const contents = Array(parseInt(req.params.howMany)).fill({ test: 'writeMany' });
    collTest.insert(contents, { w: 0, j: false })
      .then(result => res.send(result))
      .catch(err => res.send(err));
  });

  app.get('/usage/db/dropCollectionTest', async (req, res) => {
    collTest.drop()
      .then(result => res.send(result))
      .catch(err => res.send(err));
  });

  app.get('/usage/db/writeTo/:connection/:db/:collection', (req, res) => {
    const conn = req.params.connection;
    const db = req.params.db;
    const coll = req.params.collection;
    dbc[conn].db(db).collection(coll).insert({ test: 'writeTo' })
      .then(result => res.send(result))
      .catch(err => res.send(err));
  });

  app.get('/usage/db/connectionsClose', (req, res) => {
    dbc.destroy('requested by API')
      .then(result => res.send(result))
      .catch(err => res.send(err));
  });

  app.get('/benchmarks/db/insUpd/:insertOrUpdate/:from/:to', async (req, res) => {
    await benchmarkWrites(collTest, req.params.insertOrUpdate, parseInt(req.params.from), parseInt(req.params.to));
    res.send('ok');
  });

  app.get('/index', (req, res) => {
    const endpoints = expressExt.expressEndpoins(app);
    res.render('endpoints_expr', { links: endpoints, config });
  });
  return app;
};

module.exports = {
  EndPointsMgUsage,
};
