const config = require('../config');
const expressExt = require('../modules/utils/express_ext');
const { logger } = require('../modules/utils/logger');
const { ProgressStatsTest } = require('../modules/utils/delos');

const EndPointsTests = (app, dbc) => {
  // const collTest = dbc.getCollection('test', 'test');

  app.get('/tests/statsTest', async (req, res) => {
    const statsTest = async (resp) => {
      logger.info('statsTest Started');
      await ProgressStatsTest(60, logger, resp);
    };
    res.setHeader('Content-Type', 'application/json');
    res.setTimeout(120000);
    try {
      await statsTest(res);
    } catch (err) { res.send(err.message); }
  });

  app.get('/tests/nop', (req, res) => {
    res.send('ok');
  });

  app.get('/', (req, res) => {
    const endpoints = expressExt.expressEndpoins(app);
    res.render('endpoints_expr', { links: endpoints, config });
  });
  return app;
};

module.exports = {
  EndPointsTests,
};
