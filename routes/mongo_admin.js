/**
 * some mongo admin commands (most dynamically generated)
 * :@todo: continue
 *
 *
 */

const { adminCommands } = require('../modules/mongo/structures');
const { arrayToRelation } = require('../modules/mongo/pipelines/transforms');
const { opsServer, opsKill } = require('../modules/mongo/admin');


const buildEndPoints = (app, dbc) => {
  const getPath = (ep) => {
    const [epname, ...options] = Object.keys(ep);
    const pathOpt1 = () => {
      return (ep[epname] === 1) ? '' : `/:${ep[epname]}`;
    };
    const pathOptRest = () => options.map(opt => `/:${opt}`);
    // console.log('pathOptRest',[pathOpt1(), pathOptRest()])
    return `/admin/${epname}${pathOpt1()}${pathOptRest().join('')}`;
  };
  const getFunc = (ep) => {
    const command = ep;
    let target = dbc.db('admin');
    return async (req, res) => {
      const parms = req.params;
      const parmsKeys= Object.keys(parms);
      if (parmsKeys.includes('db')) {
        target = dbc.db(parms.db);
        if (parmsKeys.includes('collection')) {
          target = target.collection(parms.collection);
        }
      }
      try {
        const result = await target.command(command);
        res.send(result);
      } catch (err) {
        res.send({ error: err });
      }
    };
  };

  adminCommands.forEach((ep) => {
    const path = getPath(ep);
    const cmd = getFunc(ep);
    app.get(path, cmd);
  });
};

const EndPointsMgAdmin = (app, dbc) => {
  const mgClient = dbc.client;
  const adminDb = dbc.client.db('admin');
  buildEndPoints(app, dbc.client);

  app.get('/admin/connectionStatus', async (req, res) => {
    try {
      const dbs = await adminDb.command({ connectionStatus: 1, showPrivileges: true });
      res.send(dbs);
    } catch (err) {
      res.send({ error: err });
    }
  });

  app.get('/admin/dbstats/:dbname', async (req, res) => {
    try {
      const db = mgClient.db(req.params.dbname);
      const stats = await db.command({ dbStats: 1 });
      // console.log('stats', stats)
      res.send(stats);
    } catch (err) {
      res.send({ error: err });
    }
  });

  app.get('/admin/ops/:ns/:minMS', async (req, res) => {
    try {
      const ops = await opsServer(adminDb, new RegExp(`^${req.params.ns}`));
      res.send(ops);
    } catch (err) {
      res.send({ error: err.message });
    }
  });

  app.get('/admin/ops/:ns/:minMS', async (req, res) => {
    const results = {};
    try {
      const ops = await opsServer(adminDb, new RegExp(`^${req.params.ns}`));
      results.count = ops.length;
      results.killed = await opsKill(adminDb, ops, 0, 100);
      results.ops = ops;
      res.send(results);
    } catch (err) {
      res.send({ error: err.message });
    }
  });

  app.get('/toRelation/:dbName/:fromCollName/:toCollName/:fieldName', async (req, res) => {
    const collIn = dbc.getCollection(req.params.dbName, req.params.fromCollName);
    const match = []; // [{ $match: { username: 'nickmilon' } }];
    try {
      res.write('[');
      const pl = arrayToRelation(collIn, req.params.toCollName, req.params.fieldName, match);
      res.write(JSON.stringify(pl, null, 4));
      const results = await collIn.aggregate(pl, { comment: 'test', allowDiskUse: true }).toArray();
      res.write(JSON.stringify(results, null, 4));
      res.end(']');
    } catch (err) {
      res.end(`${err.message}]`);
    }
  });

  app.get('/admin/test', (req, res) => {
    res.send('ok');
  });
};

module.exports = {
  EndPointsMgAdmin,
};
