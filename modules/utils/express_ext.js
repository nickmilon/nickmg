/** express extentions
 *
 */

const expressEndpoins = (appExp) => {
  const getMethods = methods => Object.keys(methods).map(k => k);
  const _routes = appExp._router.stack;
  return _routes.filter(r => r.route)
    .map(r => [r.route.path, getMethods(r.route.methods)]);
};

module.exports = {
  expressEndpoins,
};
