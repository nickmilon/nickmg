
/**
 * named after  https://en.wikipedia.org/wiki/Delphi
 *
 */
const getKeysList = (obj) => {
  // returns a nested array of obj keys
  return Object.keys(obj).map((k) => {
    if (typeof (obj[k]) === 'function') {
      return k;
    }
    return [k, getKeysList(obj[k])];
  });
};

const getKeysPath = (obj, prefix, separator = '/') => {
  const isobject = x => Object.prototype.toString.call(x) === '[object Object]';
  prefix = prefix ? prefix + separator : '';
  return Object.keys(obj).reduce((result, key) => {
    if (isobject(obj[key])) {
      result = result.concat(getKeysPath(obj[key], prefix + key));
    } else {
      result.push(prefix + key);
    }
    return result;
  }, []);
};

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

const EventLogger = (eventEmmiter, onArr=[], name='', logger= console, verbose=1) => {
  onArr.forEach((on) => {
    eventEmmiter.on(on, (event) => {
      const logDetails = { eventFrom: name, on };
      if (verbose > 0) { logDetails.data=event; }
      logger.info(logDetails);
    });
  });
};

const ngrams = async (aString, minSize=3, prefixOnly=false) => {
  const rt = [];
  const ngramPrefix = (txt) => {
    for (let i = minSize; i <= Math.max(txt.length, minSize); i +=1) { rt.push(txt.slice(0, i)); }
  };
  if (prefixOnly === true) {
    ngramPrefix(aString);
  } else {
    for (let j = 0; j <= aString.length - minSize; j +=1) { ngramPrefix(aString.slice(j)); }
  }
  return rt;
};

const percent = (part, whole) => (100 * (part / whole)) - 100;

const dtAddMinutes = (dt, minutes) => new Date(dt.getTime() + (minutes * 60000));

const dtDiffms = (dtStart, dtEnd) => dtEnd.getTime() - dtStart.getTime();

const convertMS = (ms, asString = true) => {
  let s = Math.floor(ms / 1000);
  let m = Math.floor(s / 60);
  s %= 60;
  let h = Math.floor(m / 60);
  m %= 60;
  const d = Math.floor(h / 24);
  h %= 24;
  if (asString === true) { return `days:${d} hours:${h} minutes:${m} seconds:${s}`; }
  return { d, h, m, s };
};


module.exports = {
  getKeysList,
  getKeysPath,
  sleep,
  EventLogger,
  ngrams,
  percent,
  dtAddMinutes,
  dtDiffms,
  convertMS,
};
