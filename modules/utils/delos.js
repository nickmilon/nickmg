/**
 * named after  delos https://en.wikipedia.org/wiki/Delos
 */

const { convertMS, dtDiffms, sleep } = require('./delphi');
const { Readable } = require('stream');


class Progress extends Readable {
  /** @todo: refactor following guidelines:
   *    - https://nodejs.org/docs/latest/api/stream.html#stream_implementing_a_readable_stream
   *    - https://nodejs.org/en/docs/guides/backpressuring-in-streams/
   *
   *    and include an optional value to display except counter
   * @param {*} cntStart
   * @param {*} cntEnd
   * @param {*} specs
   * @param {*} everySecs
   * @param {*} name
   * @param {*} superOpts
   */
  constructor(cntStart=0, cntEnd=null, specs, everySecs=60, name='progress', superOpts=[]) {
    super(...superOpts);
    this.stats = {
      name,
      status: 'starting',
      statsRun: 0,
      dtCrt: new Date(),
      dtUpd: new Date(),
      cntStart,
      cntNow: cntStart,
      cntEnd,
      range: cntEnd - cntStart,
      opsCnt: 1,
      opsPerSec: 0,
      opsPerSecLast: 0,
      opsPerHour: 0,
      timeRun: {},
    };
    this.__live = true;
    if (this.stats.cntEnd != null) { this.stats.range = this.stats.cntEnd - this.stats.cntStart; }
    this.specs = specs;
    this.StreamLastData = '{}';
    this._read = n => this.StreamLastData;
    this.pushObj = async (obj) => {
      if (this.__live === true) {
        this.lastData = JSON.stringify(obj, null, 4);
        this.push(this.lastData);
      }
    };

    this.onStats = async () => {
      this.pushObj(await this.getStats());
    };

    this.statsInterval = setInterval(this.onStats.bind(this), everySecs * 1000);
    setTimeout(this.onStats.bind(this), 500); // first
  }

  async destroy() {
    if (this.__live === true) {
      clearInterval(this.statsInterval);
      this.statsInterval = undefined;
      this.stats.status = 'ended';
      await this.onStats();
      this.__live = false;
      this.push(null);
    }
  }

  async getStats() {
    const stats = this.stats;
    stats.statsRun += 1;

    const now = new Date();
    const runningMSLast = dtDiffms(stats.dtUpd, now);
    stats.opsPerSecLast = Math.round((this.specs.counter - stats.opsCnt) / (runningMSLast / 1000), 2);
    stats.cntNow = this.specs.counter;
    if (stats.cntNow > 1 && stats.status === 'starting') { stats.status = 'running'; }
    stats.dtUpd = now;
    stats.opsCnt = stats.cntNow - stats.cntStart;
    const runningMS = dtDiffms(stats.dtCrt, stats.dtUpd);
    stats.opsPerSec = Math.round(stats.opsCnt / (runningMS / 1000), 2);
    stats.opsPerHour = stats.opsPerSec * 60;
    stats.timeRun = convertMS(runningMS);
    if (stats.cntEnd != null) {
      stats.percDone = Math.round(100 * (stats.opsCnt / stats.range), 6);
      stats.opsRemaing = stats.range - stats.opsCnt;
      stats.remainingSecs = Math.floor(stats.opsRemaing / stats.opsPerSec);
      stats.timeRem = convertMS(stats.remainingSecs * 1000);
    }
    return stats;
  }

  static toStr(data) { return data.toString('utf8'); }
  static toObj(data) { return JSON.parse(this.toStr(data)); }
}

const ProgressStatsTest = async (loops=30, logger=console, resp) => {
  const specs = { counter: 1 };
  const stats = new Progress(0, loops, specs, 10, 'tesProgress');
  stats.pipe(resp);
  stats.on('data', async data => logger.info(Progress.toObj(data)));
  stats.pushObj({ ProgressStatsTest: 'pushObj' });
  while (specs.counter < loops) {
    specs.counter += 1;
    await sleep(1000); // eslint-disable-line
  }

  logger.info('ProgressStatsTest exiting');
  await stats.destroy();
};

module.exports = {
  Progress,
  ProgressStatsTest,
};
