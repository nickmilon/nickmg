const winston = require('winston');
const fs = require('fs');

const env = process.env.NODE_ENV || 'development';
const config = require('../../config');

const logLevel = env === 'development' ? 'debug' : 'info';
const logFile = `${process.env.HOME}/${config.application.name}.log`;

const tsFormat = () => (new Date()).toISOString();
const logger = new (winston.Logger)({
  transports: [
    // colorize the output to the console
    new (winston.transports.Console)({
      timestamp: tsFormat,
      colorize: true,
      json: false,
      // prettyPrint: true,
      // prettyPrint: function ( object ){return JSON.stringify(object, null, 4)},
      level: logLevel,
    }),
    new (winston.transports.File)({
      filename: logFile,
      timestamp: tsFormat,
      json: true,
      level: logLevel,
    }),
  ],
});

module.exports = {
  logger,
  dropLogFile: () => fs.truncate(logFile, 0),
};
