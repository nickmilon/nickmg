module.exports = {
  adminCommands: [
    { buildInfo: 1 },
    // {collStats: 'collection', scale: 'int', verbose: 'boolean'},
    { connPoolStats: 1 },
    { listDatabases: 1 },
    { logRotate: 1 },
    { ping: 1 },
    { shutdown: 1, force: 'boolean' },
    { listCollections: 1, db: null },
  ],
};
