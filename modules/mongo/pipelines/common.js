
const facet = (facetArr) => {
  /**
   * facatArr:  [ [facetName, pipeline] [facetName, pipeline] ...etc ]
   */
  return { $facet: Object.assign(...facetArr.map(d => ({ [d[0]]: d[1] }))) };
};

module.exports = {
  facet,
};
