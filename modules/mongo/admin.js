const { logger } = require('./utils');

const opsServer = async (adminDb, ns, desc, minMS) => {
  const cmdOpts = { currentOp: 1 };
  if (ns !== undefined) { cmdOpts.ns = ns; }
  if (desc!== undefined) { cmdOpts.desc = desc; }
  if (minMS!== undefined) { cmdOpts.microsecs_running = { $gt: minMS * 1000 }; }
  const ops = await adminDb.command(cmdOpts);
  const inprog = ops.inprog || [];
  inprog.sort((a, b) => b.microsecs_running - a.microsecs_running); // desceding
  return inprog;
};

const opsKill = async (adminDb, opsArr=[], minOps=1000, percent=100) => {
  if (opsArr.length > minOps) {
    const opsTokill = opsArr.slice(0, Math.round(opsArr.length * (percent / 100)));
    Promise.all(opsTokill.map((op) => {
      return adminDb.command({ killOp: 1, op: op.opid });
    }));
    return opsTokill;
  }
  return [];
};

const opsFilter = (op, { ns= 'XXXYY', desc = 'conn', minMS=NaN } = {}) => {
  // example: opsFilter (op, { ns: req.params.ns, minMS: parseFloat(req.params.minMS) });
  if (ns && ns !== op.ns) return false;
  if (desc && op.desc !== desc) return false;
  if (!minMS.isNaN && op.microsecs_running < minMS *1000) return false;
  return true;
};

/**
 * guards againnst long running operationss (kills)
 * @param {*} adminDb
 * @example setInterval(guard, 1000 * 60 * 5);
 */
const guard = async (adminDb) => {
  const results = { dt: new Date() };
  logger.log({ results });
  try {
    let ops = await opsServer(adminDb, new RegExp('^rapchat.accounts'));
    ops = ops.filter(op => op.secs_running > 2);
    results.count = ops.length;
    if (results.count > 40) {
      results.killed = await opsKill(adminDb, ops, 50, 20);
      results.ops = ops;
      logger.log(JSON.stringify(results, null, 4));
    }
  } catch (err) {
    logger.error({ err });
  }
};

module.exports = {
  opsServer,
  opsKill,
  opsFilter,
  guard,
};
