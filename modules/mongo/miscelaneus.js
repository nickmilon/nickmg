/**
 *
 *
 */

function atlasHost(
  /**
   * returns --host parameter from atlas configuration for mongorestore
   * to be used in mongodump
   *
   */
  replicaSet= 'xxxx-shard-yyyy',
  nodes = [
    'west-2-primary-prod-shard-00-00-XXXX.mongodb.net:27017',
    'west-2-primary-prod-shard-00-01-XXXX.mongodb.net:27017',
    'west-2-primary-prod-shard-00-02-XXXX.mongodb.net:27017',
  ],

) {
  return `${replicaSet}/${nodes.join(',')}`;
}

module.exports = {
  atlasHost,
};
