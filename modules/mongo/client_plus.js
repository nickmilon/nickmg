/*
* critical events
* [closed, serverClosed, serverDescriptionChanged]

*/

const MongoClient = require('mongodb').MongoClient;


class MongoClientPlus {
  constructor() {
    this.client = null;
  }
  async connect(name = 'local', connUri = 'mongodb://localhost:27017', options = {}) {
    // if connection uri is null we will try to get it from env
    const connUriToUse = (connUri === null) ? process.env[name] : connUri;
    if (connUriToUse === undefined) {
      const errEnvNF = new Error(`mongo can't connect env variable:${name} is not defided and connUri is null`);
      throw (errEnvNF);
    }

    try {
      await this.destroy(); // just in case of re-connecting
      this.name = name;
      this.client = await MongoClient.connect(connUriToUse, options);
      return this;
    } catch (err) {
      throw (err);
    }
  }
  get events() {
    // client events :Warning: undocumented
    return Object.keys(this.client.topology._events);
  }
  getCollection(dbName, collName) { return this.client.db(dbName).collection(collName); }
  async destroy(reason = 'uknown') {
    try {
      if (this.client != null) {
        await this.client.close();
        this.client = null;
        return { client: this.name, closed: reason };
      }
      return { client: this.name, was_closed: 1 };
    } catch (err) {
      return err;
    }
  }
}
// serverHeartbeatSucceeded, on=serverHeartbeatStarted serverHeartbeatStarted
module.exports = {
  MongoClientPlus,
  MongoEventsBasic: ['serverOpening', 'serverDescriptionChanged',
    'serverHeartbeatFailed', 'serverClosed', 'topologyOpening',
    'topologyClosed', 'topologyDescriptionChanged', 'joined',
    'left', 'ping', 'ha', 'authenticated', 'error', 'timeout', 'close', 'parseError',
    'open', 'fullsetup', 'all',
    'reconnect'],
  MongoEventsFreq: ['serverHeartbeatSucceeded', 'serverHeartbeatStarted'], // Frequent events
};
