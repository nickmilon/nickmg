/**
 * mongoDB utilities
 *
 */
const addDT = (document = {}, fld = '_dtCr', asTimestamp = false) => {
  if (asTimestamp === false) {
    document[fld] = new Date(); // :warning: its local time
  } else {
    document[fld] = new Date().getTime();
  }
};

module.exports = {
  addDT,
};
