/* eslint-disable no-bitwise */
/* eslint-disable no-console */


const doc1 =
{
  start_time: 1556922852465.0,
  sbc: '10.138.82.47:5060',
  coreswitch: 'lydia',
  ingress_trunk_id: 43,
  ingress_trunk_name: 'Live Test',
  ingress_carrier_id: 18,
  ingress_carrier_name: 'VoIP Essential Inc.',
  orig_ip: '104.160.177.85',
  pdd_timeout: 60,
  orig_ani: '5106352985',
  orig_dst_number: '00#15105499059',
  orig_codecs: ['G729/8000', 'PCMU/8000', 'GSM/8000', 'telephone-event/8000'],
  ingress_tech_prefix: '00#',
  orig_dnis: '15105499059',
  routing_id: 13,
  routing_name: 'LiveDynamic',
  ingress_rate_table_id: 14,
  ingress_balance: 1000000,
  term_ani: '5106352985',
  ingress_max_duration: 7200,
  ingress_decimal_points: 4,
  lrn_number: '15105499059',
  lrn_source: 'sip:lrn.summitsystemsus.com',
  ingress_rate_type: 'I',
  orig_code: '1',
  orig_code_country: 'USA',
  orig_code_name: 'USA',
  ingress_rate: 0.008,
  ingress_rate_id: 22443377,
  ingress_rate_table_name: 'VE Ratesheet',
  ingress_lrn: 0,
  ingress_bill_increment: 6,
  ingress_bill_start: 6,
  pdd: 0,
  processing: 49,
  ring_time: 0,
  duration: 0,
};

const doc2 =
{
  pdd: 4276,
  processing: 49,
  ring_time: 8013,
  response_ingress: '200 OK',
  response_ingress_code: 200,
  response_egress: '200 OK',
  response_egress_code: 200,
  release_cause: 'Answered',
  egress_trunk_trace: [{ egress_rate_id: 36390627,
    egress_trunk_id: 35,
    egress_trunk_name: 'D Piratel',
    egress_min_duration: 0,
    egress_max_duration: 7200,
    egress_rate_table_id: 235,
    egress_rate_table_name: '199_Piratel_SDW1P15RA_20190114',
    egress_lrn: 1,
    egress_carrier_id: 39,
    egress_carrier_name: 'Piratel',
    egress_bill_increment: 6,
    egress_bill_start: 6,
    term_code_country: 'USA',
    term_code_name: null,
    term_code: '1510549',
    egress_rate: 0.0013,
    egress_rate_type: 'I',
    egress_tech_prefix: '08245#',
    term_ip: '98.158.150.100',
    term_dst_number: '08245#15105499059',
    try_timeout: 2,
    pdd_timeout: 6,
    egress_decimal_points: 4,
    term_ani: '5106352985',
    ts: 1556922852518.0,
    egress_pdd: 4223,
    ring_time: 8013,
    term_codecs: ['G729/8000', 'PCMU/8000', 'telephone-event/8000'],
    response_egress_code: 200,
    response_egress: '200 OK',
    release_cause: 'Answered',
    end_ts: 1556922864754.0,
    last: 1 }],
  answer_time: 1556922864754.0,
};

const doc3 =
{ pdd: 4276,
  processing: 49,
  ring_time: 8013,
  release_side: 'O',
  response_ingress: '200 OK',
  response_ingress_code: 200,
  response_egress: '200 OK',
  response_egress_code: 200,
  release_cause: 'Answered',
  duration: 8,
  egress_duration: 8,
  ingress_billtime: 12,
  egress_billtime: 12,
  ingress_cost: 0.0016,
  egress_cost: 0.0003,
  credit: 999999.9984,
  credit_start: 1000000,
  profit: 0.0013,
  profit_percent: 433.3333,
  egress_trunk_trace: [{ egress_rate_id: 36390627,
    egress_trunk_id: 35,
    egress_trunk_name: 'D Piratel',
    egress_min_duration: 0,
    egress_max_duration: 7200,
    egress_rate_table_id: 235,
    egress_rate_table_name: '199_Piratel_SDW1P15RA_20190114',
    egress_lrn: 1,
    egress_carrier_id: 39,
    egress_carrier_name: 'Piratel',
    egress_bill_increment: 6,
    egress_bill_start: 6,
    term_code_country: 'USA',
    term_code_name: null,
    term_code: '1510549',
    egress_rate: 0.0013,
    egress_rate_type: 'I',
    egress_tech_prefix: '08245#',
    term_ip: '98.158.150.100',
    term_dst_number: '08245#15105499059',
    try_timeout: 2,
    pdd_timeout: 6,
    egress_decimal_points: 4,
    term_ani: '5106352985',
    ts: 1556922852518.0,
    egress_pdd: 4223,
    ring_time: 8013,
    term_codecs: ['G729/8000', 'PCMU/8000', 'telephone-event/8000'],
    response_egress_code: 200,
    response_egress: '200 OK',
    release_cause: 'Answered',
    end_ts: 1556922864754.0,
    last: 1 }],
  answer_time: 1556922864754.0,
  end_time: 1556922872064.0,
};

const benchmarkWrites = async (coll, insertOrUpdate='insert', from=1, to=10000, w=1, reportStep) => {
  // await coll.deleteMany({});
  if (reportStep === undefined) { reportStep = Math.round((to - from) / 10); }
  console.log('benchmarkWrites', { insertOrUpdate, from, to, reportStep });
  const startTime = new Date();
  const stats = (i) => {
    const elapsedSeconds = (new Date() - startTime) / 1000;
    const cnt = i - from;
    console.log('ops, opsPerSec', cnt, parseInt(cnt / elapsedSeconds));
    return elapsedSeconds;
  };

  const dbOp = async (cid, setDoc) => { // bug https://jira.mongodb.org/browse/SERVER-14322
    try {
      return await coll.updateOne({ call_id: cid }, { $set: setDoc }, { upsert: true, w });
    } catch (err) {
      if (err.message.startsWith('E11000')) {
        try {
          // return await coll.updateOne({ call_id: cid }, { $set: setDoc }, { upsert: true, w });
        } catch (err1) { console.log('errorXXX1', { err1 }); return false; }
      } else { console.log('errorXXX2', { err }); return false; }
    }
  };

  const getCallId = n => `FOOBAR${n}`;
  // const getCallId = n => `FOOBAR`;
  for (let i = from; i <= to; i+=1) {
    const doc = doc1;
    if (insertOrUpdate === 'insert') {
      doc.call_id = getCallId(i);
      const res1 = coll.insertOne(doc);
    } else {
      dbOp(getCallId(i), doc1);
    }
    if (i > from + 20) {
      dbOp(getCallId(i-10), doc2);
      dbOp(getCallId(i-20), doc3);
    }
    if (i % reportStep === 0 | i === to) { stats(i); }
  }
};

module.exports = {
  benchmarkWrites,
};


/*
const testUpd = (doc, from, to, reportStep) => {
  const startTime = new Date();
  const stats = () => {
    const elapsedSeconds = (new Date() - startTime) / 1000;
    const cnt = i - from;
    console.log('ops, opsPerSec', cnt, parseInt(cnt / elapsedSeconds));
    return elapsedSeconds;
  };
  for (let i = from; i <= to; i +=1) {
    doc.call_id = `foobar${i}`;
    db.nicktest.update({ call_id: doc.call_id }, { $set: doc }, { multi: false, upsert: true });
    if (i % reportStep === 0 | i === to) { stats(i) };
  }
};


const testIns = (doc, from, to, reportStep) => {
  const startTime = new Date();
  const stats = () => {
    const elapsedSeconds = (new Date() - startTime) / 1000;
    const cnt = i - from;
    console.log('ops, opsPerSec', cnt, parseInt(cnt / elapsedSeconds));
    return elapsedSeconds;
  }
  for (i = from; i <= to; i++) {
    doc.call_id = `foobar${i}`;
    db.nicktest.insert(doc);
    if (i % reportStep === 0 | i === to) { stats(i) };
  }
};
*/
