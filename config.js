/**
 *  configureation module
 *  dbConn1: [arbitrary_connection_name, mongo uri string, options]
 *  if  mongo uri is null will get foobar value from env variable arbitrary_connection_name
 *  i.e.:  export dbWork="mongodb://localhost:27617"
 */

const { name, version } = require('./package.json');


module.exports = {
  application: {
    name,
    version,
  },
  // dbConn1: ['local', 'mongodb://localhost:27617/test2', { forceServerObjectId: true, appname: 'nickmg', poolSize: 40 }],
  dbWork: ['dbWork', null, { forceServerObjectId: true, appName: 'rc', poolSize: 10 }],
  portAdmin: 3004,
  portApp: 3008,
};
